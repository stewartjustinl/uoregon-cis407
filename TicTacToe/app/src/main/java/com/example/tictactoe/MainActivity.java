package com.example.tictactoe;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;




public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button[][] buttons = new Button[3][3]; // our board is 3x3 so we need a 3x3 array

    private Boolean player1Turn = true; // sets the player1 to be the starting player

    private int countRounds; // tracks how many rounds have been played

    private int player1Points; // tracks player 1 points

    private int player2Points; // tracks player 2 points

    private TextView textViewPlayer1; // displays player 1 points

    private TextView textViewPlayer2; // displays player 1 points

    @Override
    protected void  onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewPlayer1 = findViewById(R.id.text_view_p1);
        textViewPlayer2 = findViewById(R.id.text_view_p2);

        // nested loop to cycle through our 3x3 button grid
        for (int i = 0; i < 3; i++ ){
            for (int j = 0; j < 3; j++) {
                String buttonID = "button_" + i + j;
                int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
                buttons[i][j] = findViewById(resID); // references buttons dynamically instead of assigning manually
                buttons[i][j].setOnClickListener(this); // when a button is clicked this will detect it and inforce a callback
            }
        }

        Button buttonReset = findViewById(R.id.button_reset); // used for the reset button on the board
        buttonReset.setOnClickListener(new View.OnClickListener() { // when a button is clicked this will detect it and inforce a callback
            @Override
            public void onClick(View v) {
                resetGame();
            }
        });
    }

    @Override
    public void onClick(View v){
            if (!((Button) v).getText().toString().equals("")) { // checks to see if the clicked button is a empty string. used to prevent possible clicking of used buttons
                return;
            }

            if (player1Turn) { // checks if player 1 turn and applies text based of result
                ((Button) v).setText("X");
            }
            else {
                ((Button) v).setText("O");
            }
            countRounds++;

            if (checkWin()) {

                if (player1Turn) {
                    player1Wins(); // player 1 win condition met
                }
                else {
                    player2wins(); // player 2 win condition met
                }
            }
            else if (countRounds==9) { // if all squares are full then we have a draw
                draw();
            }
            else {
                player1Turn = !player1Turn; // if no win condition met then switch player turn
            }

    }

    private Boolean checkWin() {
        String[][] field = new String[3][3];

        for (int i = 0; i < 3; i++ ){

            for (int j = 0; j < 3; j++) {
                field [i][j]= buttons[i][j].getText().toString();
            }
        }

        for (int i = 0; i < 3; i++) { // case if three matching and not empty  " rows "
            if (field[i][0].equals(field[i][1])
                    && field[i][0].equals(field[i][2])
                    && !field[i][0].equals("")){
                return true;
            }
        }

        for (int i = 0; i < 3; i++) { // case if three matching and not empty  " columns "
            if (field[0][i].equals(field[1][i])
                    && field[0][i].equals(field[2][i])
                    && !field[0][i].equals("")){
                return true;
            }
        }

        if (field[0][0].equals(field[1][1])
                && field[0][0].equals(field[2][2])
                && !field[0][0].equals("")){ // case top left to bottom right " diagonal"
            return true;
        }

        if (field[0][2].equals(field[1][1])
                && field[0][2].equals(field[2][0])
                && !field[0][2].equals("")) { // case bottom left to top right " diagonal"
            return true;
        }

        return false;
    }

    private void player1Wins() {
        player1Points++;
        Toast.makeText(this, "Player 1 wins the Game!", Toast.LENGTH_SHORT).show(); // this prints a message on the board showing the player won
        updatePointsText();
        resetBoard();
    }

    private void  player2wins() {
        player2Points++;
        Toast.makeText(this, "Player 2 wins the Game!", Toast.LENGTH_SHORT).show(); // this prints a message on the board showing the player won
        updatePointsText();
        resetBoard();

    }

    private void  draw() {
        Toast.makeText(this, " Draw, try again", Toast.LENGTH_SHORT).show(); // this prints a message on the board showing a draw
        resetBoard();
    }

    private void updatePointsText() {
        textViewPlayer1.setText(" Player 1: " + player1Points);
        textViewPlayer2.setText(" Player 2: " + player2Points);
    }

    private void resetBoard() {
        for (int i = 0; i < 3; i++ ){
            for (int j = 0; j < 3; j++) {
                buttons[i][j].setText("");
            }
        }
        countRounds = 0;
        player1Turn=true;
    }

    private void resetGame() {
        player1Points = 0;
        player2Points = 0;
        updatePointsText();
        resetBoard();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("roundCount", countRounds);
        outState.putInt("player1Points", player1Points);
        outState.putInt("player2Points", player2Points);
        outState.putBoolean("player1Turn", player1Turn);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        countRounds = savedInstanceState.getInt("roundCount");
        player1Points = savedInstanceState.getInt("player1Points");
        player2Points = savedInstanceState.getInt("player2Points");
        player1Turn = savedInstanceState.getBoolean("player1Turn");
    }
}